﻿namespace TicTacToe
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_one = new System.Windows.Forms.Button();
            this.btn_two = new System.Windows.Forms.Button();
            this.btn_three = new System.Windows.Forms.Button();
            this.btn_four = new System.Windows.Forms.Button();
            this.btn_five = new System.Windows.Forms.Button();
            this.btn_six = new System.Windows.Forms.Button();
            this.btn_seven = new System.Windows.Forms.Button();
            this.btn_eight = new System.Windows.Forms.Button();
            this.btn_nine = new System.Windows.Forms.Button();
            this.Intro = new System.Windows.Forms.Label();
            this.X_score = new System.Windows.Forms.Label();
            this.O_score = new System.Windows.Forms.Label();
            this.result_X = new System.Windows.Forms.Label();
            this.result_O = new System.Windows.Forms.Label();
            this.restart_button = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btn_one
            // 
            this.btn_one.BackColor = System.Drawing.SystemColors.Info;
            this.btn_one.Font = new System.Drawing.Font("Lucida Calligraphy", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_one.Location = new System.Drawing.Point(38, 133);
            this.btn_one.Name = "btn_one";
            this.btn_one.Size = new System.Drawing.Size(127, 79);
            this.btn_one.TabIndex = 1;
            this.btn_one.Text = "?......";
            this.btn_one.UseVisualStyleBackColor = false;
            this.btn_one.Click += new System.EventHandler(this.button_click);
            // 
            // btn_two
            // 
            this.btn_two.BackColor = System.Drawing.SystemColors.Info;
            this.btn_two.Font = new System.Drawing.Font("Lucida Calligraphy", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_two.Location = new System.Drawing.Point(207, 133);
            this.btn_two.Name = "btn_two";
            this.btn_two.Size = new System.Drawing.Size(127, 79);
            this.btn_two.TabIndex = 2;
            this.btn_two.Text = "?......";
            this.btn_two.UseVisualStyleBackColor = false;
            this.btn_two.Click += new System.EventHandler(this.button_click);
            // 
            // btn_three
            // 
            this.btn_three.BackColor = System.Drawing.SystemColors.Info;
            this.btn_three.Font = new System.Drawing.Font("Lucida Calligraphy", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_three.Location = new System.Drawing.Point(372, 133);
            this.btn_three.Name = "btn_three";
            this.btn_three.Size = new System.Drawing.Size(127, 79);
            this.btn_three.TabIndex = 3;
            this.btn_three.Text = "?......";
            this.btn_three.UseVisualStyleBackColor = false;
            this.btn_three.Click += new System.EventHandler(this.button_click);
            // 
            // btn_four
            // 
            this.btn_four.BackColor = System.Drawing.SystemColors.Info;
            this.btn_four.Font = new System.Drawing.Font("Lucida Calligraphy", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_four.Location = new System.Drawing.Point(38, 256);
            this.btn_four.Name = "btn_four";
            this.btn_four.Size = new System.Drawing.Size(127, 79);
            this.btn_four.TabIndex = 4;
            this.btn_four.Text = "?......";
            this.btn_four.UseVisualStyleBackColor = false;
            this.btn_four.Click += new System.EventHandler(this.button_click);
            // 
            // btn_five
            // 
            this.btn_five.BackColor = System.Drawing.SystemColors.Info;
            this.btn_five.Font = new System.Drawing.Font("Lucida Calligraphy", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_five.Location = new System.Drawing.Point(207, 256);
            this.btn_five.Name = "btn_five";
            this.btn_five.Size = new System.Drawing.Size(127, 79);
            this.btn_five.TabIndex = 5;
            this.btn_five.Text = "?......";
            this.btn_five.UseVisualStyleBackColor = false;
            this.btn_five.Click += new System.EventHandler(this.button_click);
            // 
            // btn_six
            // 
            this.btn_six.BackColor = System.Drawing.SystemColors.Info;
            this.btn_six.Font = new System.Drawing.Font("Lucida Calligraphy", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_six.Location = new System.Drawing.Point(372, 256);
            this.btn_six.Name = "btn_six";
            this.btn_six.Size = new System.Drawing.Size(127, 79);
            this.btn_six.TabIndex = 6;
            this.btn_six.Text = "?......";
            this.btn_six.UseVisualStyleBackColor = false;
            this.btn_six.Click += new System.EventHandler(this.button_click);
            // 
            // btn_seven
            // 
            this.btn_seven.BackColor = System.Drawing.SystemColors.Info;
            this.btn_seven.Font = new System.Drawing.Font("Lucida Calligraphy", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_seven.Location = new System.Drawing.Point(38, 375);
            this.btn_seven.Name = "btn_seven";
            this.btn_seven.Size = new System.Drawing.Size(127, 79);
            this.btn_seven.TabIndex = 7;
            this.btn_seven.Text = "?......";
            this.btn_seven.UseVisualStyleBackColor = false;
            this.btn_seven.Click += new System.EventHandler(this.button_click);
            // 
            // btn_eight
            // 
            this.btn_eight.BackColor = System.Drawing.SystemColors.Info;
            this.btn_eight.Font = new System.Drawing.Font("Lucida Calligraphy", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_eight.Location = new System.Drawing.Point(207, 375);
            this.btn_eight.Name = "btn_eight";
            this.btn_eight.Size = new System.Drawing.Size(127, 79);
            this.btn_eight.TabIndex = 8;
            this.btn_eight.Text = "?......";
            this.btn_eight.UseVisualStyleBackColor = false;
            this.btn_eight.Click += new System.EventHandler(this.button_click);
            // 
            // btn_nine
            // 
            this.btn_nine.BackColor = System.Drawing.SystemColors.Info;
            this.btn_nine.Font = new System.Drawing.Font("Lucida Calligraphy", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_nine.Location = new System.Drawing.Point(372, 375);
            this.btn_nine.Name = "btn_nine";
            this.btn_nine.Size = new System.Drawing.Size(127, 79);
            this.btn_nine.TabIndex = 9;
            this.btn_nine.Text = "?......";
            this.btn_nine.UseVisualStyleBackColor = false;
            this.btn_nine.Click += new System.EventHandler(this.button_click);
            // 
            // Intro
            // 
            this.Intro.AutoSize = true;
            this.Intro.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.Intro.Font = new System.Drawing.Font("Snap ITC", 25.8F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Intro.ForeColor = System.Drawing.Color.DarkRed;
            this.Intro.Location = new System.Drawing.Point(28, 26);
            this.Intro.Name = "Intro";
            this.Intro.Size = new System.Drawing.Size(307, 55);
            this.Intro.TabIndex = 10;
            this.Intro.Text = "Tic Tac Toe";
            // 
            // X_score
            // 
            this.X_score.AutoSize = true;
            this.X_score.BackColor = System.Drawing.Color.Transparent;
            this.X_score.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.X_score.Font = new System.Drawing.Font("Lucida Calligraphy", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.X_score.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.X_score.Location = new System.Drawing.Point(562, 43);
            this.X_score.Name = "X_score";
            this.X_score.Size = new System.Drawing.Size(158, 38);
            this.X_score.TabIndex = 11;
            this.X_score.Text = "X score:- ";
            // 
            // O_score
            // 
            this.O_score.AutoSize = true;
            this.O_score.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.O_score.Font = new System.Drawing.Font("Lucida Calligraphy", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.O_score.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.O_score.Location = new System.Drawing.Point(562, 112);
            this.O_score.Name = "O_score";
            this.O_score.Size = new System.Drawing.Size(149, 38);
            this.O_score.TabIndex = 12;
            this.O_score.Text = "O score:-";
            // 
            // result_X
            // 
            this.result_X.AutoSize = true;
            this.result_X.Font = new System.Drawing.Font("Segoe Print", 13.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.result_X.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.result_X.Location = new System.Drawing.Point(756, 43);
            this.result_X.Name = "result_X";
            this.result_X.Size = new System.Drawing.Size(0, 43);
            this.result_X.TabIndex = 13;
            // 
            // result_O
            // 
            this.result_O.AutoSize = true;
            this.result_O.Font = new System.Drawing.Font("Segoe Print", 13.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.result_O.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.result_O.Location = new System.Drawing.Point(756, 112);
            this.result_O.Name = "result_O";
            this.result_O.Size = new System.Drawing.Size(0, 43);
            this.result_O.TabIndex = 14;
            // 
            // restart_button
            // 
            this.restart_button.BackColor = System.Drawing.Color.PaleTurquoise;
            this.restart_button.Font = new System.Drawing.Font("Lucida Calligraphy", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.restart_button.ForeColor = System.Drawing.Color.Red;
            this.restart_button.Location = new System.Drawing.Point(674, 350);
            this.restart_button.Name = "restart_button";
            this.restart_button.Size = new System.Drawing.Size(123, 58);
            this.restart_button.TabIndex = 15;
            this.restart_button.Text = "Restart";
            this.restart_button.UseVisualStyleBackColor = false;
            this.restart_button.Click += new System.EventHandler(this.restart_button_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(891, 496);
            this.Controls.Add(this.restart_button);
            this.Controls.Add(this.result_O);
            this.Controls.Add(this.result_X);
            this.Controls.Add(this.O_score);
            this.Controls.Add(this.X_score);
            this.Controls.Add(this.Intro);
            this.Controls.Add(this.btn_nine);
            this.Controls.Add(this.btn_eight);
            this.Controls.Add(this.btn_seven);
            this.Controls.Add(this.btn_six);
            this.Controls.Add(this.btn_five);
            this.Controls.Add(this.btn_four);
            this.Controls.Add(this.btn_three);
            this.Controls.Add(this.btn_two);
            this.Controls.Add(this.btn_one);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "Form1";
            this.Text = " ";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_one;
        private System.Windows.Forms.Button btn_two;
        private System.Windows.Forms.Button btn_three;
        private System.Windows.Forms.Button btn_four;
        private System.Windows.Forms.Button btn_five;
        private System.Windows.Forms.Button btn_six;
        private System.Windows.Forms.Button btn_seven;
        private System.Windows.Forms.Button btn_eight;
        private System.Windows.Forms.Button btn_nine;
        private System.Windows.Forms.Label Intro;
        private System.Windows.Forms.Label X_score;
        private System.Windows.Forms.Label O_score;
        private System.Windows.Forms.Label result_X;
        private System.Windows.Forms.Label result_O;
        private System.Windows.Forms.Button restart_button;
    }
}

