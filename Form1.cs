﻿using System;
using System.Windows.Forms;

namespace TicTacToe
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public int playerXturn = 0; // checking the player X is turn only one
        public string winner = "Nobody wins";
        public int score_X, score_O;


        private void button_click(object sender, EventArgs e)
        {

            Button button_receiver = (Button)sender;

            if (button_receiver.Text == "?......")
            {

                if (playerXturn == 0)
                {
                    button_receiver.Text = "X";
                    playerXturn++;

                }

                else if (button_receiver.Text != "X" && playerXturn > 0)
                {
                    button_receiver.Text = "O";
                    playerXturn = 0; // setting the value to default

                }


            }




            if (checking_winner())
            {
                if (winner == "O")
                {
                    MessageBox.Show("O won", "WinningResult");
                    score_O++;

                    play_after_win();


                }

                else if (winner == "X")
                {
                    MessageBox.Show("X won", "WinningResult");
                    score_X++;

                    play_after_win();

                }




            }

            //  when nobody wins

            if (checking_nobody_wins())
            {

                MessageBox.Show("nobody wins");

                new_start();



            }


            // Print the score of X and O

            result_X.Text = score_X.ToString();
            result_O.Text = score_O.ToString();



            if ((score_X == score_O) && (score_X != 0) && (score_O != 0))
            {

                DialogResult result = MessageBox.Show("Draw match, Do you want to play new game?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (result == DialogResult.No)
                {

                    Application.Exit();
                }
                else
                {
                    new_start();
                }
            }


        } // end of  button_click void function

        private bool checking_winner()
        {


            // horizintal check of btn_one, btn_two,btn_three

            if (btn_one.Text != "?......" && btn_two.Text != "?......" && btn_three.Text != "?......")
            {
                if ((btn_one.Text == btn_two.Text) && (btn_one.Text == btn_three.Text) && (btn_two.Text == btn_three.Text))
                {

                    deciding_winner(btn_one.Text);
                    return true;


                }
            }
            // vertical check of btn_one, btn_four,btn_seven, 

            if (btn_one.Text != "?......" && btn_four.Text != "?......" && btn_seven.Text != "?......")
            {
                if ((btn_one.Text == btn_four.Text) && (btn_four.Text == btn_seven.Text) && (btn_one.Text == btn_seven.Text))
                {

                    deciding_winner(btn_one.Text);
                    return true;
                }
            }

            // daignal checking of btn_one, btn_five, btn_nine

            if (btn_one.Text != "?......" && btn_five.Text != "?......" && btn_nine.Text != "?......")
            {
                if ((btn_one.Text == btn_five.Text) && (btn_five.Text == btn_nine.Text) && (btn_one.Text == btn_nine.Text))
                {
                    deciding_winner(btn_one.Text);

                    return true;
                }
            }

            // vertical check of btn_three,btn_six,btn_nine

            if (btn_three.Text != "?......" && btn_six.Text != "?......" && btn_nine.Text != "?......")
            {
                if ((btn_three.Text == btn_six.Text) && (btn_six.Text == btn_nine.Text) && (btn_three.Text == btn_nine.Text))
                {
                    deciding_winner(btn_three.Text);

                    return true;
                }
            }
            // dialong check of btn_three, btn_five, btn_seven


            if (btn_three.Text != "?......" && btn_five.Text != "?......" && btn_seven.Text != "?......")
            {

                if ((btn_three.Text == btn_five.Text) && (btn_five.Text == btn_seven.Text) && (btn_three.Text == btn_seven.Text))
                {
                    deciding_winner(btn_three.Text);
                    return true;
                }
            }

            // horizontal check of btn_four , btn_five , btn_six

            if (btn_four.Text != "?......" && btn_five.Text != "?......" && btn_six.Text != "?......")
            {

                if ((btn_four.Text == btn_five.Text) && (btn_four.Text == btn_six.Text) && (btn_five.Text == btn_six.Text))
                {
                    deciding_winner(btn_four.Text);

                    return true;

                }
            }


            // horizontal check of btn_seven , btn_eight , btn_nine

            if (btn_seven.Text != "?......" && btn_eight.Text != "?......" && btn_nine.Text != "?......")
            {
                if ((btn_seven.Text == btn_eight.Text) && (btn_eight.Text == btn_nine.Text) && (btn_seven.Text == btn_nine.Text))
                {
                    deciding_winner(btn_seven.Text);

                    return true;

                }
            }


            // vertical checking of btn_two, btn_five, btn_eight 

            if (btn_two.Text != "?......" && btn_five.Text != "?......" && btn_eight.Text != "?......")
            {
                if ((btn_two.Text == btn_five.Text) && (btn_five.Text == btn_eight.Text) && (btn_two.Text == btn_eight.Text))
                {

                    deciding_winner(btn_two.Text);
                    return true;
                }
            }


            return false;

        } //end of checking_winner function


        private void new_start()
        {

            MessageBox.Show("New game", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information);

            // setting the all  value to default zero
            playerXturn = 0;
            score_X = 0;
            score_O = 0;
            result_O.Text = "0";
            result_X.Text = "0";
            btn_one.Text = btn_two.Text = btn_three.Text = btn_four.Text = btn_five.Text = btn_six.Text = btn_seven.Text = btn_eight.Text = btn_nine.Text = "?......";



        }
        private void play_after_win()
        {

            MessageBox.Show("New turn", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information);

            // setting the all  value to default zero
            playerXturn = 0;

            winner = "Nobody wins";
            btn_one.Text = btn_two.Text = btn_three.Text = btn_four.Text = btn_five.Text = btn_six.Text = btn_seven.Text = btn_eight.Text = btn_nine.Text = "?......";



        }

        private bool checking_nobody_wins()
        {

            // checking after all value filled, Is that checking_winner return false
            if (btn_one.Text != "?......" && btn_two.Text != "?......" && btn_three.Text != "?......" && btn_four.Text != "?......" && btn_five.Text != "?......" && btn_six.Text != "?......" && btn_seven.Text != "?......" && btn_eight.Text != "?......" && btn_nine.Text != "?......")
            {

                if (checking_winner() == false)
                {

                    return true;
                }


            } // end of main if statement
            return false;
        }
        private void deciding_winner(string value) // checking whose wins 
        {

            winner = value;


        }

        private void restart_button_Click(object sender, EventArgs e)
        {
            new_start();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    } // end of class
} // end of namespace

